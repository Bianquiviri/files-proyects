<?php

namespace Database\Factories;

use App\Models\fileVersion;
use Illuminate\Database\Eloquent\Factories\Factory;

class FileVersionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = fileVersion::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' =>'test.'
        ];
    }
}
