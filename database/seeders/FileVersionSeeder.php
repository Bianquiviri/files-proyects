<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FileVersionSeeder extends Seeder
{
    /**
     * Run the database seeds for files.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            ['name' => 'test.text', 'files_id' => '1'],
            ['name' => 'test.text', 'files_id' => '1'],
            ['name' => 'recipes.doc', 'files_id' => '2'],
            ['name' => 'recipes.doc', 'files_id' => '2'],
            ['name' => 'picture.png', 'files_id' => '3'],
            ['name' => 'photos,png', 'files_id' => '3'],
        ];

        DB::table('file_versions')->insert($data);

    }


}
