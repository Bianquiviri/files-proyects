<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * for verios of files
     *
     * @return void
     */

    public function run()
    {
        $data = [
            ['name' => 'the-file-id-1', 'id' => '1'],
            ['name' => 'the-file-id-2', 'id' => '2'],
            ['name' => 'the-file-id-3', 'id' => '3'],
            ['name' => 'the-file-id-4', 'id' => '4'],
        ];
        DB::table('files')->insert($data);
    }
}
