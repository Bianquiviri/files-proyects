#!/usr/bin/env bash
#git checkout dev ;
rm -rf vendor;
composer install;
cp .env.example .env;
./vendor/bin/sail php artisan key:generate   ;
./vendor/bin/sail php artisan migrate:refresh --seed;
./vendor/bin/sail up -d;
#./vendor/bin/sail php artisan test;

#python -mwebbrowser -n http://triviabackend-app.test/api/categories
python -mwebbrowser -n http://127.0.0.1/api/files
#php artisan dump-autoload ;

