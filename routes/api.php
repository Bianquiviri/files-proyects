<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('files', \App\Http\Controllers\FilesController::class)
                ->only(['index','update','store','show']);

Route::view('/welcome', 'welcome', ['name' => 'Taylor']);


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
