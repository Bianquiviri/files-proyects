<?php

namespace App\Models;

use FilesVersionMigration;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class files extends Model
{
    use HasFactory;
    protected $fillable = ['id', 'name', 'create_at', 'update_at'];
    protected $hidden = ['create_at','update_at'];

    public function getVersionfiles()
    {
        return $this->hasMany(fileVersion::class,'files_id');

    }
}
