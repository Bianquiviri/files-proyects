<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class fileVersion extends Model
{
    use HasFactory;

    protected $fillable = ['id', 'name', 'files_id', 'created_at', 'updated_at'];
    protected $hidden = ['updated_at', 'deleted_at','created_at','files_id'];

    public function file()
    {
        return $this->belongsTo(files::class, 'files_id');
    }
}
