<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class filesRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'The name of the file in required',
        ];
    }
}
