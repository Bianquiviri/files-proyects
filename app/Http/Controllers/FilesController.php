<?php

namespace App\Http\Controllers;

use App\Http\Requests\filesRequest;
use App\Models\files;
use App\Models\fileVersion;
use Illuminate\Routing\Controller;
use phpDocumentor\Reflection\File;

class FilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // to show the files in the database
    public function index()
    {
        try {
            $files = files::get();
            foreach ($files as $file) {
                $datafile[] = [
                    'id' => $file->name,
                    'versions' => fileVersion::where('files_id', $file->id)->get()
                ];
            }
            return response()->json($datafile, 200);
        } catch (\Exception $ext) {
            return response()->json(['error', $ext->getMessage(), 500]);
        }
    }
    /*
  *  to update a new version of the file
  */
    public function update(filesRequest $request, $file)
    {
        try {

            $files = files::findOrfail($file);
            $version = new fileVersion();
            $version->files_id = $files->id;
            $version->name = $request->name;
            $version->save();

            return response()->json(['sucess' => 'the file was update'], 200);
        } catch (\Exception $ext) {
            return response()->json(['error', $ext->getMessage()], 500);
        }
    }


    // to store a new file whit the frist vesrion of the file
    public function store(filesRequest $request)
    {
        try {
            // Fixme:  i have to chance this work but i dont like

            $file = new files();
            $file->name = $request->name;
            $file->save();

            $fileupdate = files::findOrFail($file->id);
            $fileupdate->name = 'the-file-id-' . $file->id;
            $fileupdate->save();

            // to store the version
            $version = new fileVersion();
            $version->files_id = $file->id;
            $version->name = $request->name;
            $version->save();

            return response()->json(['sucess' => 'file store', 'id_file' => $file->id]);
        } catch (\Exception $ext) {
            return response()->json(['error', $ext->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\files $files
     * @return \Illuminate\Http\Response
     */

    /**
     * @param files $file
     * @return \Illuminate\Http\JsonResponse
     * to show one record version
     */
    public function show(files $file)
    {
        try {
            $files = files::find($file);
            foreach ($files as $file) {
                $datafile = [
                    'id' => $file->name,
                    'versions' => fileVersion::where('files_id', $file->id)->get()
                ];
            }
            return response()->json($datafile, 200);
        } catch (\Exception $ext) {
            return response()->json(['error', $ext->getMessage()], 500);
        }
    }

    /*
     * to stor a query
     */
    public function stort($query, $order)
    {
        try {

            $files = files::get();
            foreach ($files as $file) {
                $datafile = [
                    'id' => $file->name,
                    'versions' => fileVersion::where(['name', 'like', '%' . $query . '%'], ['files_id', $file->id])
                        ->orderBy('name', $order)->get()
                ];
            }
            return response()->json($datafile, 200);
        } catch (\Exception $ext) {
            return response()->json(['error', $ext->getMessage()], 500);
        }
    }
}
